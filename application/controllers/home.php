<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	//Ini untuk halaman depan (Single page map)

	public function __construct() {
		parent::__construct();
		$this->load->model('home_model');
	}

	public function login() {
		//Halaman login admin
		if ($_POST) { //jika POST Method
			if (($this->input->post('username') == $this->config->item('username_admin')) && ($this->input->post('password') == $this->config->item('password_admin'))) {
				$this->session->set_userdata(array('isLogin'=> true));
				redirect('admingis');
			}
			else {
				$data['gagal'] = "true";
				$this->load->view('admin/login',$data);
			}
		} else { //jika GET Method
			$this->load->view('admin/login');
		}
	}

	public function index() {
		$data['menu'] = $this->home_model->generateMenu();
		$this->load->view('home/index_baru',$data);
	}

	public function testingModel() {
		var_dump($this->home_model->generateMenu());
	}
}
