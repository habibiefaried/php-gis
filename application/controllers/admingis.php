<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admingis extends CI_Controller {
	//Ini untuk halaman Admin

	/* 1 PR : Gk bisa delete file .kmz */

	public function __construct() {
		parent::__construct();
		if (!$this->session->userdata('isLogin')) redirect('home/login');
		else $this->load->model('admin');
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect('admingis/login');
	}

	public function index() {
		$this->load->view('admin/index');
	}

	public function ManajemenGrup() {
		$this->load->view('admin/grup');
	}

	public function ManajemenGIS() {
		$this->load->view('admin/layer');
	}

	public function tambahlayer() {
		if ($_POST) {
			$config['upload_path'] = './geo_data/';
			$config['allowed_types'] = "*";
			$config['encrypt_name'] = FALSE;
			$config['overwrite'] = FALSE;
			

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload()) {
				echo $this->upload->display_errors('<p>', '</p>');
				echo "<a href=".site_url()."admingis/tambahlayer>Kembali</a>";
			}else{
				/*
				$data = array('upload_data' => $this->upload->data());
				var_dump($data); */
				$data_upload = $this->upload->data(); 
				$name_file = $data_upload['file_name'];
				//echo $name;
				$this->admin->insertLayer($this->input->post('nama_layer'),$this->input->post('deskripsi'),$name_file);
				redirect('admingis/ManajemenGIS');
			}
		}
		else $this->load->view('admin/tambahlayer');
	}

	public function editlayer($id) {
		if ($_POST) {
			$config['upload_path'] = './geo_data/';
			$config['allowed_types'] = "*";
			$config['encrypt_name'] = FALSE;
			$config['overwrite'] = FALSE;
			

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload()) {
				if ($this->upload->error_msg[0] == "You did not select a file to upload.") {
					$this->admin->editLayerNonUpload($id,$this->input->post('nama_layer'),$this->input->post('deskripsi'));
					redirect('admingis/ManajemenGIS');
				}else {
					echo $this->upload->display_errors('<p>', '</p>');
					echo "<a href=".site_url()."admingis/editlayer>Kembali</a>";
				}
			}else{
				/*
				$data = array('upload_data' => $this->upload->data());
				var_dump($data); */
				$data_layer = $this->upload->data();
				$name_file = $data_layer['file_name'];
				//echo $name;
				$this->admin->editLayer($id,$this->input->post('nama_layer'),$this->input->post('deskripsi'),$name_file);
				redirect('admingis/ManajemenGIS');
			}
		}else $this->load->view('admin/editlayer',array("data"=>$this->admin->getSpesificLayer($id)));
	}

	public function deletelayer() { //API Delete (hanya berlaku POST)
		if ($_POST) {
			$this->admin->deleteLayer($this->input->post('id'));
			echo json_encode("OK");
		}
	}

	public function tambahgroup() {
		if ($_POST) {
			//var_dump(json_encode($_POST['layers']));
			$this->admin->insertGroup($this->input->post('nama_grup'),json_encode($this->input->post('layers')));
			redirect('admingis/ManajemenGrup');
		}else $this->load->view('admin/tambahgrup',array("layers"=>$this->admin->getAllGIS()));
	}

	public function editgroup($id) {
		if ($_POST) {
			$this->admin->editGroup($id,$this->input->post('nama_grup'),json_encode($this->input->post('layers')));
			redirect('admingis/ManajemenGrup');
		}else $this->load->view('admin/editgrup',array("layers"=>$this->admin->getAllGIS(), 
							       "data"=>$this->admin->getSpesificGroup($id)));
	}
	
	public function deletegroup() { //API Delete (hanya berlaku POST)
		if ($_POST) {
			$this->admin->deleteGroup($this->input->post('id'));
			echo json_encode("OK");
		}
	}
	//API untuk admin - menggunakan datatable

	public function ManajemenGISAPI() {
		$iDisplayStart = $this->input->post('iDisplayStart');
		$iDisplayLength = $this->input->post('iDisplayLength');
		$sEcho = $this->input->post('sEcho');
		$iSortCol_0 = $this->input->post('iSortCol_0');
		$sSortDir_0 = $this->input->post('sSortDir_0');
		$sSearch = $this->input->post('sSearch');

		$query = $this->admin->getAllGIS($iDisplayLength,$iDisplayStart,$sSearch, $this->admin->GISMapping($iSortCol_0),$sSortDir_0);

		$return = array();
		$return['sEcho'] = intval($sEcho);
		$return['iTotalRecords'] = $this->admin->getAllGISCount($sSearch);
		$return['iTotalDisplayRecords'] = $return['iTotalRecords'];

		$hasil = array();
		$no = 1;
		foreach ($query->result() as $row) {
			$data = array();
			$data[] = "<input type='checkbox' onclick='addDelete(\"".$row->id."\");'>&nbsp".$no;
			$data[] = $row->nama_layer;
			
			if ($row->isPoint == 0) { $data[] = $row->deskripsi; $data[] = $row->lokasi;}
			else {
				$temp = json_decode($row->deskripsi);
				if (is_array($temp[0])) $temp[0]="<font style='color:red'>Deskripsi NULL</font>";
				$data[] = $temp[0]."..dst";
				$temp = json_decode($row->coordinates);
				if (is_array($temp[0])) $temp[0]="<font style='color:red'>LatLong NULL</font>";
				$data[] = $temp[0]."..dst";
			}
			if ($row->isPoint==0) {
				$data[] = "<span style='color:blue'>No</span>";
				$data[] = '<i class="fa fa-edit"><a href="'.site_url().'admingis/editlayer/'.$row->id.'">Edit</a></i>&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-trash-o"><a href="#" onclick="deleteLayer(\''.$row->id.'\'); return false;">Delete</a></i>';
			}
			else {
				$data[] = "<span style='color:green'>Yes</span>";
				$data[] = '<i class="fa fa-edit"><a href="'.site_url().'admingis/editlayerpoint/'.$row->id.'">Edit</a></i>&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-trash-o"><a href="#" onclick="deleteLayer(\''.$row->id.'\'); return false;">Delete</a></i>';
			}			
			array_push($hasil,$data);
			$no++;
		}

		$return['aaData'] = $hasil;
		echo json_encode($return);
	}

	public function ManajemenGroupAPI() {
		$iDisplayStart = $this->input->post('iDisplayStart');
		$iDisplayLength = $this->input->post('iDisplayLength');
		$sEcho = $this->input->post('sEcho');
		$iSortCol_0 = $this->input->post('iSortCol_0');
		$sSortDir_0 = $this->input->post('sSortDir_0');
		$sSearch = $this->input->post('sSearch');

		$query = $this->admin->getAllGroup($iDisplayLength,$iDisplayStart,$sSearch, $this->admin->GroupMapping($iSortCol_0),$sSortDir_0);

		$return = array();
		$return['sEcho'] = intval($sEcho);
		$return['iTotalRecords'] = $this->admin->getAllGroupCount($sSearch);
		$return['iTotalDisplayRecords'] = $return['iTotalRecords'];

		$hasil = array();
		$no = 1;
		foreach ($query->result() as $row) {
			$data = array();
			$data[] = $no;
			$data[] = $row->nama_group;
			$data[] = '<i class="fa fa-edit"><a href="'.site_url().'admingis/editgroup/'.$row->id.'">Edit</a></i>&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-trash-o"><a href="#" onclick="deleteGroup(\''.$row->id.'\'); return false;">Delete</a></i>';
			array_push($hasil,$data);
			$no++;
		}

		$return['aaData'] = $hasil;
		echo json_encode($return);
	}

	public function deleteMultipleLayer() {
		$layers = $this->input->post('layers');
		if ($layers != "") {
			foreach ($layers as $key => $row) {
				if ($row == true) $this->admin->deleteLayer($key);
			}
			echo json_encode("OK");
		} else echo json_encode("OK");
	}

	
	public function EditHeader() {
		$this->load->view('admin/editheader');
	}

	public function UploadHeader() {
		$config['upload_path'] = './';
		$config['allowed_types'] = 'png|jpg|jpeg';
		$config['file_name'] = "header.png";
		$config['overwrite'] = TRUE;	
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload()) echo "Error : ".$this->upload->display_errors();
		else redirect("admingis/EditHeader");
	}

	public function tambahlayerpoint() {
		if ($_POST) {
			$config['upload_path'] = './icon/';
			$config['allowed_type'] = '*';
			$config['encrypt_name'] = FALSE;
			$config['overwrite'] = FALSE;
			

			$this->load->library('upload', $config);
			
			$coordinates = $this->admin->parseXML($this->input->post('kml-code'));
			if (!$coordinates) exit("<script>
						function goBack() {
						    window.history.back()
						}
						</script>
						Kesalahan pada kode XML.<br><button onclick='goBack()'>Kembali</button>");
			else {
				if (!$this->upload->do_upload()) {
					if ($this->upload->error_msg[0] == "You did not select a file to upload.") {
						$this->admin->addLayerPoint($this->input->post('nama_layer'),$coordinates,"default.png");
						redirect('admingis/ManajemenGIS');
					}else {
						echo $this->upload->display_errors('<p>', '</p>');
						echo "<script>
							function goBack() {
							    window.history.back()
							}
							</script>
							Kesalahan pada kode XML.<br><button onclick='goBack()'>Kembali</button>";
					}
				}else{
					$data_layer = $this->upload->data();
					$name_file = $data_layer['file_name'];
					$this->admin->addLayerPoint($this->input->post('nama_layer'),$coordinates,$name_file);
					redirect('admingis/ManajemenGIS');
				}
			}

		}else $this->load->view('admin/tambahlayerpoint');
	}

	public function editlayerpoint($id){
		if ($_POST){ 
			if ($this->input->post('kml-code') == "") {
				$this->admin->editLayerPoint($id,$this->input->post('nama_layer'),"");
			}
			else {
				$coordinates = $this->admin->parseXML($this->input->post('kml-code'));
				if (!$coordinates) exit("<script>
							function goBack() {
							    window.history.back()
							}
							</script>
							Kesalahan pada kode XML.<br><button onclick='goBack()'>Kembali</button>");
				else $this->admin->editLayerPoint($id,$this->input->post('nama_layer'),$coordinates);
			}
			redirect('admingis/ManajemenGIS');
		}else {
			$this->db->select('*')->from('layer')->where('id', $id);
			$data['row'] = $this->db->get()->row();
			$data['id_form'] = $id;
			$this->load->view('admin/editlayerpoint',$data);
		}
	}
}
