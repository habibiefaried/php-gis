 </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src="<?=base_url();?>template/js/jquery-1.10.2.js"></script>
    <script src="<?=base_url();?>template/js/bootstrap.min.js"></script>
    <script src="<?=base_url();?>template/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script>
	function UrlExists(url, cb){
		    jQuery.ajax({
			url:      url,
			dataType: 'text',
			type:     'GET',
			complete:  function(xhr){
			    if(typeof cb === 'function')
			       cb.apply(this, [xhr.status]);
			}
		    });
		}

	function readKML(){
		var url = $("#url_kml").val();
		//alert(url);
		$("#kml-url-help").html('<img src="<?=base_url();?>loading.gif">');
		UrlExists(url,function(status) {
			if (status == 200) {
				$.get(url, function(data) {
				   $("#kml-url-help").html('Load KML Selesai :)');
				   $("#kml-code").val(data);
				}, 'text');
			}else { $("#kml-url-help").html('URL tidak valid :('); alert("File KML tidak ditemukan pada url "+url);}
		});
	}	
	</script>
    <!-- Page-Level Plugin Scripts - Blank -->

    <!-- SB Admin Scripts - Include with every page -->
    <script src="<?=base_url();?>template/js/sb-admin.js"></script>

    <!-- Page-Level Demo Scripts - Blank - Use for reference -->

</body>

</html>
