<?=$this->load->view('admin/header');?>
            <h1>Tambah Layer Grup</h1><br>
		<?php $attributes = array('role' => 'form'); ?>
		<?php echo form_open('admingis/tambahgroup',$attributes);?>

              <div class="form-group">
                <label>Nama Grup</label>
                <input class="form-control" placeholder="Isikan teks" name="nama_grup">
                <p class="help-block">Nama Grup yang merepresentasikan beberapa layer</p>
              </div>

              <div class="form-group">
                <label>Pilihan Layer</label>
		<?php
		foreach ($layers->result() as $row) {
		?>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="layers[]" value="<?=$row->id;?>">
                    <?=$row->nama_layer;?>
                  </label>
		</div>
		<?php } ?>
              </div>

	      <div class="form-group">
                 <button type="submit" class="btn btn-primary">Submit</button>
              </div>
	</form>
<?=$this->load->view('admin/footer');?>
