<?=$this->load->view('admin/header');?>
            <h1>Tambah Layer (khusus KML Point)</h1><br>
		<?php $attributes = array('role' => 'form'); ?>
		<?php echo form_open_multipart('admingis/tambahlayerpoint',$attributes);?>

              <div class="form-group">
                <label>Nama Layer</label>
                <input class="form-control" placeholder="Isikan teks" name="nama_layer" required>
                <p class="help-block">Nama Layer / Daerah</p>
              </div>

	      <div class="form-group">
                <label>Read KML from URL&nbsp;&nbsp;&nbsp;</label>
                <input class="" placeholder="Isikan URL" name="url_kml" id="url_kml" width="10px">&nbsp;&nbsp;&nbsp;
		<button onclick="readKML(); return false;" class="btn btn-info">Baca KML</button>
                <p class="help-block" id="kml-url-help">URL tempat KML berada (contoh http://localhost/phpgis/doc.kml). <b>Tidak harus diisi</b></p>
              </div>

              <div class="form-group">
                <label>Sintaks KML</label>
                <textarea class="form-control" rows="6" name="kml-code" id="kml-code"></textarea>
		<p class="help-block">Wajib mengikuti format (contoh ada di <a href="<?=base_url();?>contoh.txt" target="_blank">sini</a>)</p>
              </div>

		<div class="form-group">
                <label>File Icon</label>
                <input type="file" name="userfile"><span>Mohon upload icon point anda. Kosongkan apabila ingin default</span>
              </div>

	      <div class="form-group">
		<a href="<?=site_url();?>admingis/ManajemenGIS"><button type="button" class="btn btn-danger">Kembali</button></a>
                 <button type="submit" class="btn btn-primary">Submit</button>
              </div>
	</form>
<?=$this->load->view('admin/footer');?>
