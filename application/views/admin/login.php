<!DOCTYPE html>
<html lang="en">
    <head>
		<meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <title>Login Site</title>
        <meta name="description" content="Custom Login Form Styling with CSS3" />
        <meta name="keywords" content="css3, login, form, custom, input, submit, button, html5, placeholder" />
        <meta name="author" content="Codrops" />
        <link rel="stylesheet" type="text/css" href="<?=base_url();?>/template/login_style/css/style.css" />
		<script src="<?=base_url();?>/template/login_style/js/modernizr.custom.63321.js"></script>
		<!--[if lte IE 7]><style>.main{display:none;} .support-note .note-ie{display:block;}</style><![endif]-->
    </head>
    <body>
        <div class="container">
			<header>
				<h1>Website <strong>Login</strong></h1>	
				<?php if(isset($gagal)) echo "<h2 style='color:red'>Username / Password Salah</h2>"; ?>	
			</header>
			
			<section class="main">
				 <?php $attributes = array('class' => 'form-1'); ?>
      				  <?=form_open('home/login', $attributes);?>
					<p class="field">
						<input type="text" name="username" placeholder="Username">
						<i class="icon-user icon-large"></i>
					</p>
						<p class="field">
							<input type="password" name="password" placeholder="Password">
							<i class="icon-lock icon-large"></i>
						</p>
					<br>
					<p class="submit">
						<button type="submit" name="submit"><i class="icon-arrow-right icon-large"></i></button>
					</p>
				<?=form_close()?>
			</section>
        </div>
    </body>
</html>
