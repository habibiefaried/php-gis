<?=$this->load->view('admin/header');?>
<link href="<?=base_url();?>template/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
            <h2></h2>
	    <a href = "<?=site_url();?>admingis/tambahgroup"><button type="button" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;Tambah Group</button></a>
		<h2></h2>
	<div class="panel panel-default">
		<div class="panel-heading">
		   Manajemen Grup Layer
		</div>
		<div class="panel-body">
            <div class="table-responsive"><br>
              <table class="display table table-bordered table-hover" id="example">
                <thead>
                  <tr>
                    <th width=10%>No</th>
                    <th>Nama Group</th>
		    <th width=15%>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
	</div></div>
<?=$this->load->view('admin/footer');?>
<script src="<?=base_url();?>template/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="<?=base_url();?>template/js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script>
	var oTable1;
	$(document).ready(function() {
		//$('#example').dataTable();
		oTable1 = $('#example').dataTable({
			"bServerSide": true,
			"bProcessing": true,
		    	"sAjaxSource": "<?=site_url();?>admingis/ManajemenGroupAPI",
		    	"sServerMethod": "POST",
			"aoColumnDefs": [
			  		{ 'bSortable': false, 'aTargets': [2] }
				],
			"aLengthMenu": [[5, 10, 25, -1], [5, 10, 25, "All"]],
        		"iDisplayLength": 5,
		});
	});

	function deleteGroup(id) {
		var r=confirm("Apakah anda yakin ingin menghapus grup ini?");
		if (r==true)  {
			$.post("<?=site_url();?>admingis/deletegroup", {id:id})
			.done(function(data) {
				//alert("Anda telah menghapus "+data);
				$("#example").dataTable().fnDraw();
			});
		}
		else return false;
	}
</script>
