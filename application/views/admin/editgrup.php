<?=$this->load->view('admin/header');?>
            <h1>Edit Layer Grup</h1><br>
		<?php $attributes = array('role' => 'form'); ?>
		<?php $query = $data->row(); ?>
		<?php echo form_open('admingis/editgroup/'.$query->id,$attributes);?>

              <div class="form-group">
                <label>Nama Grup</label>
                <input class="form-control" placeholder="Isikan teks" name="nama_grup" value="<?=$query->nama_group;?>">
                <p class="help-block">Nama Grup yang merepresentasikan beberapa layer</p>
              </div>
		<?php
		if ($query->layers == "") return false;
		else {
			$arr = json_decode($query->layers);
			//var_dump($arr);
			function checkLayer($name,$arr) {
				if ($arr != "") {
					if (in_array($name, $arr)) return true;
					else return false;
				}else return false;
			}
		}
		?>
              <div class="form-group">
                <label>Pilihan Layer</label>
		<?php
		foreach ($layers->result() as $row) {
		?>
                <div class="checkbox">
                  <label>
			<?php
			if (checkLayer($row->id,$arr)) {
			?>
			    <input type="checkbox" name="layers[]" value="<?=$row->id;?>" checked>
			    <?=$row->nama_layer;?>
			<?php } else { ?>
			     <input type="checkbox" name="layers[]" value="<?=$row->id;?>">
			    <?=$row->nama_layer;?>
			<?php } ?>
                  </label>
		</div>
		<?php } ?>
              </div>

	      <div class="form-group">
                 <button type="submit" class="btn btn-primary">Submit</button>
              </div>
	</form>
<?=$this->load->view('admin/footer');?>
