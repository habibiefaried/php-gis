<?=$this->load->view('admin/header');?>
            <h1>Tambah Layer</h1><br>
		<?php $attributes = array('role' => 'form'); ?>
		<?php echo form_open_multipart('admingis/tambahlayer',$attributes);?>

              <div class="form-group">
                <label>Nama Layer</label>
                <input class="form-control" placeholder="Isikan teks" name="nama_layer">
                <p class="help-block">Nama Layer / Daerah</p>
              </div>

              <div class="form-group">
                <label>Deskripsi</label>
                <textarea class="form-control" rows="3" name="deskripsi"></textarea>
              </div>

		<div class="form-group">
                <label>File KMZ / KML</label>
                <input type="file" name="userfile">
		<p class="help-block">Maksimal 2 MB, <span style="color:red">tidak berfungsi</span> apabila KMZ/KML berupa point</p>
              </div>

	      <div class="form-group">
		<a href="<?=site_url();?>admingis/ManajemenGIS"><button type="button" class="btn btn-danger">Kembali</button></a>
                 <button type="submit" class="btn btn-primary">Submit</button>
              </div>
	</form>
<?=$this->load->view('admin/footer');?>
