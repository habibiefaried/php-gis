<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>PHP GIS Admin</title>

    <!-- Core CSS - Include with every page -->
    <link href="<?=base_url();?>template/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url();?>template/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Blank -->

    <!-- SB Admin CSS - Include with every page -->
    <link href="<?=base_url();?>template/css/sb-admin.css" rel="stylesheet">

</head>

<body>

    <div id="wrapper">

        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?=site_url();?>admingis">GIS Admin</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Administrator <b class="caret"></b></a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="<?=site_url();?>admingis/logout"><i class="fa fa-power-off"></i> Log Out</a></li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="side-menu">
                           <li><a href="<?=site_url();?>admingis/ManajemenGIS"><i class="fa fa-desktop"></i> Manajemen GIS</a></li> 
            		   <li><a href="<?=site_url();?>admingis/ManajemenGrup"><i class="fa fa-desktop"></i> Manajemen Grup</a></li>
			   <li><a href="<?=site_url();?>admingis/EditHeader"><i class="fa fa-desktop"></i> Edit Header</a></li>
                    </ul>
                    <!-- /#side-menu -->
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper" style="min-height:650px;">
            <div class="row">
                <div class="col-lg-12">
