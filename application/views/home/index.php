<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Web Geographic Information System</title>

    <!-- Bootstrap core CSS -->
   <link href="<?=base_url();?>template/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url();?>template/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Blank -->

    <!-- SB Admin CSS - Include with every page -->
    <link href="<?=base_url();?>template/css/sb-admin.css" rel="stylesheet">

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    <script src="<?=base_url();?>template/js/jquery-1.10.2.js"></script>
    <script src="<?=base_url();?>template/js/bootstrap.min.js"></script>
    <script src="<?=base_url();?>template/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Blank -->

    <!-- SB Admin Scripts - Include with every page -->
    <script src="<?=base_url();?>template/js/sb-admin.js"></script>

    <script>
var map;
var information_pool = new Array();
function initialize() {
  //var pwk = new google.maps.LatLng(107.5000,6.4167);
  var pwk = new google.maps.LatLng(-6.5569400,107.4433300);
  var mapOptions = {
    zoom: 11,
    center: pwk
  }

  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

  /*
  var ctaLayer = new google.maps.KmlLayer({
    url: 'http://phpgis.habibiefaried.com/geo_test/deskel.kmz'
  });
	
	google.maps.event.addListener(ctaLayer, "metadata_changed", function() {
    		alert("Sample map sudah diload");
	});
	
  ctaLayer.setMap(map); */
}

google.maps.event.addDomListener(window, 'load', initialize);

    </script>

  </head>
  <body>
	
    <div id="wrapper">
      <!-- Sidebar -->

      <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
          <a class="navbar-brand" href="index.html">Web GIS Kota Purwakarta &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id="test" style="font-size: 12px; display: none;"><img src="http://phpgis.habibiefaried.com/loadinfo.gif"><span id="msg_load"></span></span></a>
        </div>

        <div class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="side-menu">
	<?php
	foreach ($menu as $name => $row) {
	?>
	<li>
	    <a href="#"><i class="fa fa-files-o fa-fw"></i><?=$name;?><span class="fa arrow"></span></a>
               <ul class="nav nav-second-level">
		<?php
		$subcount = count($row["nama_layer"]);
		//echo $subcount;
		for ($i=0;$i<$subcount;$i++) {
	        ?>
                <li><a href="#" class="layer-process" onclick="processLayer('<?=$row['id'][$i];?>','<?=$row['lokasi_layer'][$i];?>');"><input type="checkbox" id="checkbox<?=$row['id'][$i];?>"> <?=$row['nama_layer'][$i];?></a></li>
		<?php
		} ?>
              </ul>
            </li>
	<?php 
	} ?>
	 </ul>
	</div></div>
	<script>
		$("#test").hide();
		$('.layer-process').click(function (e) {
			e.stopPropagation();
			//e.preventDefault();
			//$(this).parent().parent().parent().addClass('open');
		});
		function processLayer(id,lokasi_map) {
			if (typeof information_pool[id] == 'undefined') {
				$('#checkbox'+id).prop('checked', true);
				$("#msg_load").html("Loading layer...");
				var lokasi_full_map = "<?=base_url();?>geo_data/"+lokasi_map;
				//alert(lokasi_full_map);
				$("#test").show();
				var pwkLayer = new google.maps.KmlLayer({
				    url: lokasi_full_map
				  });
				pwkLayer.setMap(map);

				google.maps.event.addListener(pwkLayer, "metadata_changed", function() {
			    		//alert("Sudah selesai loading");
					$("#test").hide();
				});
				//console.log("ID Clicked : "+ID);
				information_pool[id] = pwkLayer;
			} else { 
				$("#msg_load").html("Deleting layer...");
				$("#test").show();
				information_pool[id].setMap(null);
				delete information_pool[id];
				$("#test").hide();
				$('#checkbox'+id).prop('checked', false);
			//alert("Hilangkan gan");
			}
		}
	</script>
	<ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b class="caret"></b></a>
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
      </nav>
		<div id="map-canvas" style="width: 1127px;height: 590px"></div>
    </div><!-- /#wrapper -->
    <!-- JavaScript -->
  </body>
</html>
