<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	private $datakolomGIS = array("layer.id","layer.nama_layer","layer.deskripsi","layer.id","layer.lokasi");	
	private $datakolomGroup = array("group_layer.id","group_layer.nama_group","group_layer.layers");	

	public function addLayerPoint($nama_layer,$coordinates,$icon) {
		$data = array(
			'nama_layer' => $nama_layer ,
			'coordinates' => $coordinates['coor_point'],
			'deskripsi' => $coordinates['desk_point'],
			'isPoint' => 1,
			'icon' => $icon,
		);

		$this->db->insert('layer', $data);
	}

	public function editLayerPoint($id,$nama_layer,$coordinates) {
		$data;
		if ($coordinates == "") {
			$data = array(
			'nama_layer' => $nama_layer,
			);
		} else {
			$data = array(
			'nama_layer' => $nama_layer,
			'coordinates' => $coordinates['coor_point'],
			'deskripsi' => $coordinates['desk_point'],
			);
		}
		$this->db->where('id',$id);
		$this->db->update('layer',$data);
	}

	public function parseXML($str) {
		//Mengembalikan array of JSON
		libxml_use_internal_errors(true);
		$xml = simplexml_load_string(strtolower($str));
		//exit($str);
		if (!$xml) { exit("Bukan XML. Silahkan cek Kode KML anda di <a href='http://xmlvalidation.com/' target='_blank'>sini</a>"); 
			return false; }
		else {
			$json = json_encode($xml);
			//var_dump($json);
			$array = json_decode($json,TRUE);
			if (array_key_exists('document',$array)) { 
				if (array_key_exists('folder',$array['document'])) {
					if (array_key_exists('placemark',$array['document']['folder'])) {
						$coor_point = array();
						$desk_point = array();

						if (array_key_exists("name",$array["document"]["folder"]["placemark"])) { //khusus untuk satu
							$desk_point[] = $array["document"]["folder"]["placemark"]["name"];
							$coor_point[] = $array["document"]["folder"]["placemark"]["point"]["coordinates"];
						}else {
							foreach ($array["document"]["folder"]["placemark"] as $row) {
								if (!is_array($row["name"])) $desk_point[] = $row["name"];
								else $desk_point[] = "Nama Kosong";

								if (!is_array($row["point"]["coordinates"])) $coor_point[] = $row["point"]["coordinates"];
								else $coor_point[] = "107.5000,-6.4167";
							}
						}
						$hasil = array();
						$hasil['coor_point'] = json_encode($coor_point);
						$hasil['desk_point'] = json_encode($desk_point);

						return $hasil;
					} else { //exit("placemark"); 
						return false; }
				} else { //exit("folder"); 
					return false; }
			} else { //exit ("document") ; 
				return false; }
		}
	}

	public function insertLayer($nama_layer,$deskripsi,$lokasi) {
		$data = array(
		   'nama_layer' => $nama_layer,
		   'deskripsi' => $deskripsi,
		   'lokasi' => $lokasi
		);

		$this->db->insert('layer', $data); 
	}

	private function deleteKMZFile($lokasi) { //MASIH gk bener
		//delete file (MASIH gk bener)
		delete_files('./geo_data/'.$lokasi);
	}

	public function editLayer($id,$nama_layer,$deskripsi,$lokasi) {
		//delete dulu KMZnya
		$query = $this->db->get_where('layer', array('id' => $id));
		$lokasi_file = $query->row()->lokasi;
		$this->deleteKMZFile($lokasi_file); //delete KMZ

		$data = array(
		       'nama_layer' => $nama_layer,
		       'deskripsi' => $deskripsi,
		       'lokasi' => $lokasi
		    );
		$this->db->where('id', $id);
		$this->db->update('layer', $data); 
	}

	public function editLayerNonUpload($id,$nama_layer,$deskripsi) {
		$data = array(
		       'nama_layer' => $nama_layer,
		       'deskripsi' => $deskripsi
		    );
		$this->db->where('id', $id);
		$this->db->update('layer', $data); 
	}
	
	public function editGroup($id,$nama_group,$layers) { //BELUM DICOBA
		$data = array(
		       'nama_group' => $nama_group,
		       'layers' => $layers
		    );
		$this->db->where('id', $id);
		$this->db->update('group_layer', $data); 
	}

	public function deleteLayer($id) {
		//delete dulu KMZnya
		$query = $this->db->get_where('layer', array('id' => $id));
		$lokasi_file = $query->row()->lokasi;
		$this->deleteKMZFile($lokasi_file); //delete KMZ
		$this->db->delete('layer',array('id'=>$id));
	}

	public function deleteGroup($id) {
		$this->db->delete('group_layer', array('id' => $id)); 
	}

	public function insertGroup($nama_group,$layers) {
		$data = array(
		   'nama_group' => $nama_group,
		   'layers' => $layers
		);

		$this->db->insert('group_layer', $data);
	}

	/* Khusus untuk pendukung API */
	public function getSpesificLayer($id) {
		return $this->db->get_where('layer', array('id' => $id));
		/*$ret = $query->row();
		return $ret->campaign_id; */		
	}

	public function getSpesificGroup($id) {
		return $this->db->get_where('group_layer', array('id' => $id));
	}

	public function getAllGIS($limit="-1",$offset="-1", $global_like="", $column_sort="layer.id",$jenis_sort="DESC") {
		$this->db->select("*");
		$this->db->from('layer');
		
		foreach ($this->datakolomGIS as $row) {
			$this->db->or_like($row,$global_like);
		}

		$this->db->order_by($column_sort,$jenis_sort);
		if (($limit!=-1) && ($offset!=-1)) $this->db->limit($limit, $offset);

		return $this->db->get();
	}

	public function GISMapping($id) {
		return $this->datakolomGIS[$id];
	}

	public function getAllGISCount($global_like="") {
		$this->db->select("*");
		$this->db->from('layer');
		
		foreach ($this->datakolomGIS as $row) {
			$this->db->or_like($row,$global_like);
		}

		return $this->db->count_all_results();
	}

	/* Manajemen grup */
	public function getAllGroup($limit="-1",$offset="-1", $global_like="", $column_sort="group_layer.id",$jenis_sort="DESC") {
		$this->db->select("*");
		$this->db->from('group_layer');
		
		foreach ($this->datakolomGroup as $row) {
			$this->db->or_like($row,$global_like);
		}

		$this->db->order_by($column_sort,$jenis_sort);
		if (($limit!=-1) && ($offset!=-1)) $this->db->limit($limit, $offset);

		return $this->db->get();
	}

	public function GroupMapping($id) {
		return $this->datakolomGroup[$id];
	}

	public function getAllGroupCount($global_like="") {
		$this->db->select("*");
		$this->db->from('group_layer');
		
		foreach ($this->datakolomGroup as $row) {
			$this->db->or_like($row,$global_like);
		}

		return $this->db->count_all_results();
	}
}
