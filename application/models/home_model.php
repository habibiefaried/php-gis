<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	public function generateMenu() {
		//generate menu layer untuk halaman depan
		$ret = array();
		$query = $this->db->get('group_layer');
		foreach ($query->result() as $row) {
			$ret[$row->nama_group] = array();
			$ret[$row->nama_group]["nama_layer"] = array();
			$ret[$row->nama_group]["lokasi_layer"] = array();
			$layers = json_decode($row->layers); //decode ke array
			if ($layers!="") { 
				foreach ($layers as $baris_layer) {
					$hasil = $this->db->get_where('layer',array('id'=>$baris_layer))->row();
					if ($hasil != null) { //null validation
						$ret[$row->nama_group]["nama_layer"][] = $hasil->nama_layer;
						$ret[$row->nama_group]["lokasi_layer"][] = $hasil->lokasi; //lokasi_layer
						$ret[$row->nama_group]["coordinates"][] = $hasil->coordinates;
						$ret[$row->nama_group]["id"][] = $hasil->id;
						$ret[$row->nama_group]["icon"][] = $hasil->icon;
						$ret[$row->nama_group]["isPoint"][] = $hasil->isPoint;
						$ret[$row->nama_group]["deskripsi"][] = $hasil->deskripsi;
					}
				}
			}
		}
		return $ret;
	}
}
